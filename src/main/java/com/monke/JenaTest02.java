/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monke;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.util.FileManager;

/**
 *
 * @author mene
 */
public class JenaTest02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Model model = FileManager.get().loadModel("data/data.ttl", "TURTLE");
        String queryString
                = "PREFIX foaf: <http://xmlns.com/foaf/0.1/> "
                + "SELECT ?name WHERE { "
                //                + " ?/person foaf:mbox <mailto:alice@example.org> . "
                //                + " ?person foaf:knows "+  + " . "
                + " ?person foaf:knows/foaf:name ?name . "
                + "}";
        Query query = QueryFactory.create(queryString);
        QueryExecution qexec = QueryExecutionFactory.create(query, model);
        try {
            ResultSet results = qexec.execSelect();
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution();
                Literal name = soln.getLiteral("name");
                System.out.println(" knows : " + name);
                System.out.println("-----------");
            }
        } finally {
            qexec.close();
        }
    }

}

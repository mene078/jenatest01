/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monke;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.FileManager;
import java.util.UUID;

/**
 *
 * @author mene
 */
public class JenaTest01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Unique ID : " + UUID.randomUUID().toString());

        Model model = FileManager.get().loadModel("data/data.ttl", "TURTLE");

        StmtIterator iter = model.listStatements();
        Integer count = 0;
        try {
            while (iter.hasNext()) {
                Statement stmt = iter.next();
                System.out.println("Subject : " + stmt.getSubject());
                System.out.println("\t : " + stmt.getSubject().getLocalName());
                System.out.println("Predicate : " + stmt.getPredicate());
                System.out.println("\t : " + stmt.getPredicate().getLocalName());
                System.out.println("Object : " + stmt.getObject());
                System.out.println("\t : " + stmt.getObject().getModel());

                System.out.println("Alt : " + stmt.getAlt());
                System.out.println("--------");
                count++;
            }
            System.out.println("count : " + count);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (iter != null) {
                iter.close();
            }
        }
        /*
         Resource cat = ResourceFactory.createResource(":dog");

         model.add(cat, RDF.type, FOAF.Person);
         model.add(cat, FOAF.name, "hakyeon");
         model.add(cat, FOAF.nick, "hy");

         model.write(System.out, "N3""N-Triples""RDF/XML""TURTLE");
         */
    }

}


import java.io.File;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mene
 */
public class CustomFilterForJena extends javax.swing.filechooser.FileFilter {

    @Override
    public String getDescription() {
        // This description will be displayed in the dialog,
        // hard-coded = ugly, should be done via I18N
        return "Jena supported file (*.ttl, *.nt, *.nq)";
    }

    @Override
    public boolean accept(File file) {
        // Allow only directories, or files with ".ttl", ".nt", ".nq" extension
        return file.isDirectory() || file.getAbsolutePath().endsWith(".ttl") || file.getAbsolutePath().endsWith(".nt") || file.getAbsolutePath().endsWith(".nq");
    }

}
